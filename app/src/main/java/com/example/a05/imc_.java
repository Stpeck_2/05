package com.example.a05;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class imc_ extends MainActivity {
    EditText editnombre;
    EditText editaltura;
    EditText editpeso;
    private Button btnlimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imc);
        editnombre = findViewById(R.id.editnombre);
        editaltura = findViewById(R.id.editaltura);
        editpeso = findViewById(R.id.editpeso);
        btnlimpiar = (Button) findViewById(R.id.btnlimpiar);
    }
    public void imc(View vista){
        String nombre = editnombre.getText().toString();
        String altura = editaltura.getText().toString();
        String peso = editpeso.getText().toString();
        if (nombre.equals("") || altura.equals("") || peso.equals("")){
            Toast.makeText(getApplicationContext(), "Por favor llene los datos", Toast.LENGTH_LONG).show();
        }else{
            String nmb = String.valueOf(nombre);
            //int nmb = Integer.parseInt(nombre);
            float alt = Float.parseFloat(altura);
            float kg = Float.parseFloat(peso);
            alt= alt/100;
            //Proceso de calcular el indice de masa corporal
            float imc=kg/(float)Math.pow(alt,2);
            //Convirtiendo el resultado a texto
            String imcTexto = String.format("%.1f", imc);
            if (imc < 18.5f)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted tiene Desnutricion",
                        Toast.LENGTH_LONG).show(); // Desnutrición
            if (imc >= 18.5f && imc < 25)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted esta Normal",
                        Toast.LENGTH_LONG).show(); // Normal
            if (imc >= 25 && imc < 30)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted tiene Sobrepeso",
                        Toast.LENGTH_LONG).show(); //Sobrepeso
            if (imc >= 30 && imc < 35)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted tiene Obesidad Grado 1",
                        Toast.LENGTH_LONG).show(); // Obestdad Grado1
            if (imc >= 35 && imc < 40)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted tiene Obesidad Grado 2",
                        Toast.LENGTH_LONG).show(); // Obesidod Grado2
            if (imc >= 40)
                Toast.makeText(getApplicationContext(), "nombre: " + nmb +
                                " y su indice de masa corporal es de: " + imcTexto+" usted tiene Obesidad Grado 3",
                        Toast.LENGTH_LONG).show(); // Obesidod Grado3
        }
        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editnombre.setText("");
                editaltura.setText("");
                editpeso.setText("");

            }
        });

    }

}
